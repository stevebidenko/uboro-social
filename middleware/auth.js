const FB = require('fb'),
  usersDB = require('../models/users'),
  express = require('express'),
  auth = express.Router();

FB.options({version: 'v2.4'});

auth.get('/list', function (req, res) {
  usersDB.list().then(function(data) {
    res.status(200).json(data);
  }).catch(function() {
    res.status(500).send('server error');
  });
});

auth.get('/lookup/:token', function(req, res) {
  if (req.params.token) {
    getFBProfile(req.params.token).then(localData => {
      res.status(200).json(localData);
    }).catch(error => {
      res.status(401).json(error);
    });
  } else {
    res.status(400).send('Bad request');
  }
});

function getFBProfile(token) {
  return new Promise(function(resolve, reject) {
    
    FB.api('me', { fields: ['id', 'name'], access_token: token }, res => {
      if (!res || res.error) {
        reject(res.error ? res.error : 'error occured while retrieving profile');
      } else {
        console.log(res);
        usersDB.lookup(res.id, token).then(backToken => {
          resolve(backToken);
        }).catch(error => {
          reject(error);
        });
      }
    });
  });
}

module.exports = auth;