(function(w, d) {
  w.loginWithFacebook = function() {
    FB.login(function(response) {
      if (response.status === 'connected') {
        passAuthResponseToAPI(response.authResponse);
      }
    });
  };
  
  w.checkLoginState = function() {
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        passAuthResponseToAPI(response.authResponse);
      }
    });
  };
  
  function passAuthResponseToAPI(authResponse) {
    var request2API = new XMLHttpRequest();

    console.log(authResponse);
    request2API.addEventListener("load", responseAPI);
    request2API.addEventListener("error", function(error) {
      console.log(error);
    });
    request2API.open("GET", "/api/v1/auth/lookup/" + authResponse.accessToken);
    request2API.send();
  }
  
  function responseAPI() {
    console.log(this.responseText);
  }
}(window, document));