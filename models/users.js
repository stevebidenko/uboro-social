var users = [
  {id: "1", name: "Serhii Borovykov", position: "founder"},
  {id: "2", name: "Alexander Musienko", position: "Tech Leader"},
  {id: "1619654364777676", name: "Stas Bidenko", position: "Javascript Developer"}
];

module.exports = {
  lookup: function(id, token) {
    return new Promise(function(resolve, reject) {
      var foundUser = users.find(user => user.id == id);

      if (foundUser) {
        foundUser.token = token;
        resolve(token);
      } else {
        reject('not found');
      }
    });
  }
};