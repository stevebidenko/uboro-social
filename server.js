// Load required modules
const http = require('http'),         // http server core module
  bodyParser = require('body-parser'),
  path = require('path'),
  express = require('express'),               // web framework external module
  auth = require('./middleware/auth'),
  serverConfiguration = {
    port: 3000
  },
  httpApp = express();

// Try to read server.conf file
try {
  serverConfiguration = JSON.parse(require('fs').readFileSync('server.conf'));
} catch (e) {
  console.warn('The file server.conf is not exist or incorrect. Will use default values');
}

httpApp.set('views', path.join(__dirname, 'views'));
httpApp.set('view engine', 'html');
httpApp.use(bodyParser.json());
httpApp.use(bodyParser.urlencoded({
  extended: false
}));

// CORS
httpApp.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, X-Password, Content-Type, Accept');
  next();
});

// RestAPI
httpApp.use('/api/v1/auth', auth);
httpApp.use('/confirmed', function(req, res) {
  console.log('Confirmed ', req.query);
  res.status(200).json(true);
});

// Setup and configure Express http server. Expect a subfolder called 'site' to be the web root.
httpApp.use(express.static(path.join(__dirname, 'client')));

// catch 404 and forward to error handler
httpApp.use(function(req, res, next) {
  const err = new Error('Not Found');

  err.status = 404;
  next(err);
});

// Start Express http server on the port
var extPort = process.env.PORT || serverConfiguration.port,
  server = http.createServer(httpApp);

server.listen(extPort, process.env.IP || "0.0.0.0", function() {
  console.info('Starting http server on the port ' + extPort);
  console.info('Ctrl-C to abort');
});
